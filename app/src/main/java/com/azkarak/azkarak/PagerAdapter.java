package com.azkarak.azkarak;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class PagerAdapter extends FragmentPagerAdapter {

    private int tabCount;
    private Bundle bundleOne, bundleTwo ;
    static AzkarFragment azkar ;
    static AhadethFragment ahadeth ;

    public PagerAdapter(FragmentManager fm, int tabCount, Bundle bundleOne, Bundle bundleTwo) {
        super(fm);
        this.tabCount= tabCount ;
        this.bundleOne = bundleOne ;
        this.bundleTwo = bundleTwo ;
    }

    @Override
    public Fragment getItem(int position) {

        switch(position){
            case 0:
                azkar = new AzkarFragment() ;
                azkar.setArguments(bundleOne);
                return azkar ;
            case 1:
                ahadeth = new AhadethFragment() ;
                ahadeth.setArguments(bundleTwo);
                return ahadeth ;
        }

        return null ;

    }

    public Bundle getBundle(Bundle bundle){
        return bundle;
    }

    @Override
    public int getCount() {
        return tabCount;
    }

    public static void changeAzkarAndAhadeth(String word){
        ahadeth.changeList(word) ;
        azkar.changeList(word) ;
    }

}
