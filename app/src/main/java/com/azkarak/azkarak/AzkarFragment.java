package com.azkarak.azkarak;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AzkarFragment extends Fragment {

    private ArrayList<Zekr> azkar ;
    private ArrayList<Zekr> searchedAzkar ;
    private AzkarAdapter azkarAdapter ;
    private ListView azkarListView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment1,container,false);

        azkarListView = view.findViewById(R.id.ahadeth_fragment_listview);

        String azkarAsString ;
        if (getArguments() != null) {
            Type listOfazkar = new TypeToken<List<Zekr>>() {
            }.getType();
            azkarAsString = getArguments().getString("AZKAR");
            azkar = new Gson().fromJson(azkarAsString, listOfazkar) ;
            searchedAzkar = new Gson().fromJson(azkarAsString, listOfazkar) ;
        }

        azkarAdapter = new AzkarAdapter(getActivity(), azkar);
        azkarListView.setAdapter(azkarAdapter);

        azkarListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent zekrIntent = new Intent(view.getContext(),ZekrActivity.class);
                zekrIntent.putExtra("ZEKR",searchedAzkar.get(position));

                startActivity(zekrIntent);

            }
        });

        return  view;
    }

    public void changeList(String word){

        searchedAzkar = search(word);
        azkarAdapter = new AzkarAdapter(getActivity(), searchedAzkar);
        azkarListView.setAdapter(azkarAdapter);

    }

    public ArrayList<Zekr> search(String word) {

        if(TextUtils.isEmpty(word))
            return  new ArrayList<>(azkar) ;

        ArrayList<Zekr> mySearchedArrayList = new ArrayList<>() ;
        char[] searchWord = word.toCharArray();
        ArrayList<char[]> elementsCharArrayList = new ArrayList<>();

        searchedAzkar.clear();
        searchedAzkar = new ArrayList<>(azkar) ;

        int arraySize = searchedAzkar.size();
        for (int i = 0; i < arraySize ; i++) {
            String x = searchedAzkar.get(i).getTitleNoTa4kel();
            elementsCharArrayList.add(x.toCharArray());
        }

        char[][] elementsCharList = elementsCharArrayList.toArray(new char[0][]);

        for (int i = 0; i < arraySize ; i++) {

            int x = 0;
            for (int j = 0; j < searchedAzkar.get(i).getTitleNoTa4kel().length() ; j++) {

                if (arabicCheck(searchWord[x], elementsCharList[i][j])) {
                    if (x == word.length() - 1) {
                        mySearchedArrayList.add(searchedAzkar.get(i));
                        break;
                    }
                    x++;
                } else {
                    x = 0;
                }
            }
        }
        return mySearchedArrayList ;

    }

    private String normalize(String input){

        //Remove honorific sign
        input=input.replaceAll("\u0610", "");//ARABIC SIGN SALLALLAHOU ALAYHE WA SALLAM
        input=input.replaceAll("\u0611", "");//ARABIC SIGN ALAYHE ASSALLAM
        input=input.replaceAll("\u0612", "");//ARABIC SIGN RAHMATULLAH ALAYHE
        input=input.replaceAll("\u0613", "");//ARABIC SIGN RADI ALLAHOU ANHU
        input=input.replaceAll("\u0614", "");//ARABIC SIGN TAKHALLUS

        //Remove koranic anotation
        input=input.replaceAll("\u0615", "");//ARABIC SMALL HIGH TAH
        input=input.replaceAll("\u0616", "");//ARABIC SMALL HIGH LIGATURE ALEF WITH LAM WITH YEH
        input=input.replaceAll("\u0617", "");//ARABIC SMALL HIGH ZAIN
        input=input.replaceAll("\u0618", "");//ARABIC SMALL FATHA
        input=input.replaceAll("\u0619", "");//ARABIC SMALL DAMMA
        input=input.replaceAll("\u061A", "");//ARABIC SMALL KASRA
        input=input.replaceAll("\u06D6", "");//ARABIC SMALL HIGH LIGATURE SAD WITH LAM WITH ALEF MAKSURA
        input=input.replaceAll("\u06D7", "");//ARABIC SMALL HIGH LIGATURE QAF WITH LAM WITH ALEF MAKSURA
        input=input.replaceAll("\u06D8", "");//ARABIC SMALL HIGH MEEM INITIAL FORM
        input=input.replaceAll("\u06D9", "");//ARABIC SMALL HIGH LAM ALEF
        input=input.replaceAll("\u06DA", "");//ARABIC SMALL HIGH JEEM
        input=input.replaceAll("\u06DB", "");//ARABIC SMALL HIGH THREE DOTS
        input=input.replaceAll("\u06DC", "");//ARABIC SMALL HIGH SEEN
        input=input.replaceAll("\u06DD", "");//ARABIC END OF AYAH
        input=input.replaceAll("\u06DE", "");//ARABIC START OF RUB EL HIZB
        input=input.replaceAll("\u06DF", "");//ARABIC SMALL HIGH ROUNDED ZERO
        input=input.replaceAll("\u06E0", "");//ARABIC SMALL HIGH UPRIGHT RECTANGULAR ZERO
        input=input.replaceAll("\u06E1", "");//ARABIC SMALL HIGH DOTLESS HEAD OF KHAH
        input=input.replaceAll("\u06E2", "");//ARABIC SMALL HIGH MEEM ISOLATED FORM
        input=input.replaceAll("\u06E3", "");//ARABIC SMALL LOW SEEN
        input=input.replaceAll("\u06E4", "");//ARABIC SMALL HIGH MADDA
        input=input.replaceAll("\u06E5", "");//ARABIC SMALL WAW
        input=input.replaceAll("\u06E6", "");//ARABIC SMALL YEH
        input=input.replaceAll("\u06E7", "");//ARABIC SMALL HIGH YEH
        input=input.replaceAll("\u06E8", "");//ARABIC SMALL HIGH NOON
        input=input.replaceAll("\u06E9", "");//ARABIC PLACE OF SAJDAH
        input=input.replaceAll("\u06EA", "");//ARABIC EMPTY CENTRE LOW STOP
        input=input.replaceAll("\u06EB", "");//ARABIC EMPTY CENTRE HIGH STOP
        input=input.replaceAll("\u06EC", "");//ARABIC ROUNDED HIGH STOP WITH FILLED CENTRE
        input=input.replaceAll("\u06ED", "");//ARABIC SMALL LOW MEEM

        //Remove tatweel
        input=input.replaceAll("\u0640", "");

        //Remove tashkeel
        input=input.replaceAll("\u064B", "");//ARABIC FATHATAN
        input=input.replaceAll("\u064C", "");//ARABIC DAMMATAN
        input=input.replaceAll("\u064D", "");//ARABIC KASRATAN
        input=input.replaceAll("\u064E", "");//ARABIC FATHA
        input=input.replaceAll("\u064F", "");//ARABIC DAMMA
        input=input.replaceAll("\u0650", "");//ARABIC KASRA
        input=input.replaceAll("\u0651", "");//ARABIC SHADDA
        input=input.replaceAll("\u0652", "");//ARABIC SUKUN
        input=input.replaceAll("\u0653", "");//ARABIC MADDAH ABOVE
        input=input.replaceAll("\u0654", "");//ARABIC HAMZA ABOVE
        input=input.replaceAll("\u0655", "");//ARABIC HAMZA BELOW
        input=input.replaceAll("\u0656", "");//ARABIC SUBSCRIPT ALEF
        input=input.replaceAll("\u0657", "");//ARABIC INVERTED DAMMA
        input=input.replaceAll("\u0658", "");//ARABIC MARK NOON GHUNNA
        input=input.replaceAll("\u0659", "");//ARABIC ZWARAKAY
        input=input.replaceAll("\u065A", "");//ARABIC VOWEL SIGN SMALL V ABOVE
        input=input.replaceAll("\u065B", "");//ARABIC VOWEL SIGN INVERTED SMALL V ABOVE
        input=input.replaceAll("\u065C", "");//ARABIC VOWEL SIGN DOT BELOW
        input=input.replaceAll("\u065D", "");//ARABIC REVERSED DAMMA
        input=input.replaceAll("\u065E", "");//ARABIC FATHA WITH TWO DOTS
        input=input.replaceAll("\u065F", "");//ARABIC WAVY HAMZA BELOW
        input=input.replaceAll("\u0670", "");//ARABIC LETTER SUPERSCRIPT ALEF

        return input;
    }

    private String normalize2(String input){

        Pattern p = Pattern.compile("[\\p{P}\\p{Mn}]");
        Matcher m = p.matcher(input);

        return m.replaceAll("");
    }

    //a function to check the Arabic Characters
    public Boolean arabicCheck(Character first, Character second) {

        int x = (int) first ;
        int y = (int) second ;

        Boolean a = x == y ;

        // أ case for
        Boolean b = ( x == 1569 || x == 1570 || x == 1571 || x == 1573 || x == 1574 || x == 1575
                || x == 1649 || x == 1650 || x == 1651 || x == 1652 || x == 1653 )
                && ( y == 1569 || y == 1570 || y == 1571 || y == 1573 || y == 1574 || y == 1575
                || y == 1649 || y == 1650 || y == 1651 || y == 1652 || y == 1653 ) ;

        // ي case for
        Boolean c = ( x == 1568 || x == 1574 || x == 1597 || x == 1598 || x == 1599 || x == 1609
                || x == 1610 || x == 1656 || x == 1740 || x == 1741 || x == 1742 || x == 1744 || x == 1745 )
                &&  ( y == 1568 || y == 1574 || y == 1597 || y == 1598 || y == 1599 || y == 1609
                || y == 1610 || y == 1656 || y == 1740 || y == 1741 || y == 1742 || y == 1744 || y == 1745 ) ;

        // و case for
        Boolean d = ( x == 1572 || x == 1608 || x == 1654 || x == 1655 || x == 1732 || x == 1733
                || x == 1734 || x == 1735 || x == 1736 || x == 1737 || x == 1738 || x == 1739 || x == 1743 )
                &&  ( y == 1572 || y == 1608 || y == 1654 || y == 1655 || y == 1732 || y == 1733
                || y == 1734 || y == 1735 || y == 1736 || y == 1737 || y == 1738 || y == 1739 || y == 1743 ) ;

        // ة case for
        Boolean e = ( x == 1577 || x == 1607 || x == 1726 || x == 1728 || x == 1729 || x == 1730
                || x == 1731 || x == 1749 || x == 1791 )
                &&  ( y == 1577 || y == 1607 || y == 1726 || y == 1728 || y == 1729 || y == 1730
                || y == 1731 || y == 1749 || y == 1791 ) ;

        return  a || b || c || d || e ;

    }

}
