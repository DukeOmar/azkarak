package com.azkarak.azkarak;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public int category = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setTitle("Azkarak");


        final Intent intent = new Intent(MainActivity.this, ForZekrAndHadethFragmentActivity.class);

        final String fourtiesAsString = getIntent().getExtras().getString("FOURTIES");
        final String azkarAsString = getIntent().getExtras().getString("AZKAR");

        intent.putExtra("AZKAR", azkarAsString);
        intent.putExtra("FOURTIES", fourtiesAsString);


        TextView azkar = findViewById(R.id.azkar);
        azkar.setOnClickListener(new View.OnClickListener() {

            // The code in this method will be executed when the azkar View is clicked on.
            @Override
            public void onClick(View view) {
                category = 0;
                intent.putExtra("category", category);
                startActivity(intent);
            }
        });

        TextView forties = findViewById(R.id.forties);
        forties.setOnClickListener(new View.OnClickListener() {

            // The code in this method will be executed when the numbers View is clicked on.
            @Override
            public void onClick(View view) {
                category = 1;
                intent.putExtra("category", category);
                startActivity(intent);

            }
        });


    }
}
