package com.azkarak.azkarak;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.google.gson.Gson;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class zekrPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<Doaa> doaas;

    public zekrPagerAdapter(FragmentManager fm, ArrayList<Doaa> doaas) {
        super(fm);
        this.doaas = doaas;

    }

    @Override
    public int getCount() {
        return this.doaas.size();
    }


    @Override
    public Fragment getItem(int position) {

        DoaaFragment doaaFragment = new DoaaFragment() ;
        Bundle bundle = new Bundle() ;
        String doaaAsString = new Gson().toJson(doaas.get(position), Doaa.class) ;
        bundle.putString("DOAA", doaaAsString);
        bundle.putString("DOAAS_NUMBER", String.valueOf(doaas.size()));
        doaaFragment.setArguments(bundle);

        return doaaFragment;
    }

}
