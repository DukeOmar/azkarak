package com.azkarak.azkarak;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class AzkarAdapter extends ArrayAdapter{

    public AzkarAdapter(Activity context, ArrayList<Zekr> zekrs) {
        // Here, we initialize the ArrayAdapter's internal storage for the context and the list.
        // the second argument is used when the ArrayAdapter is populating a single TextView.
        // Because this is a custom adapter for two TextViews and an ImageView, the adapter is not
        // going to use this second argument, so it can be any value. Here, we used 0.
        super(context, 0, zekrs);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Check if the existing view is being reused, otherwise inflate the view
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.row_1, parent, false);
        }


        // Get the {@link Forty} object located at this position in the list
        Zekr currentZekr = (Zekr) getItem(position);

        // Find the TextView in the list_item.xml layout with the ID version_name
        TextView nameTextView = listItemView.findViewById(R.id.raw1_text_view);
        // Get the version name from the current AndroidFlavor object and
        // set this text on the name TextView
        nameTextView.setText(currentZekr.getTitle());


        // Find the ImageView in the list_item.xml layout with the ID raw1
        ImageView iconView = listItemView.findViewById(R.id.raw1_image_view);

        if (currentZekr.getFavourite() == 0) {
            // Get the image resource ID from the current forty object and
            // set the image to iconView
            iconView.setImageResource(R.drawable.islamic_pattern_2);
        } else if (currentZekr.getFavourite() == 1) {
            iconView.setImageResource(R.drawable.islamic_pattern_3);
        }

        // Return the whole list item layout (containing 2 TextViews and an ImageView)
        // so that it can be shown in the ListView


        return listItemView;


    }

}
