package com.azkarak.azkarak;

import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

public class HadethActivity extends AppCompatActivity {

    private Forty hadeth;
    private TextView hadethText;
    private TextView hadethTeller;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ahadeth_inner_layout);

        //For back button
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        hadethText = findViewById(R.id.hadeth_text);
        hadethTeller = findViewById(R.id.hadeth_teller);


        if (getIntent().hasExtra("hadeth")) {

            hadeth = (Forty) getIntent().getSerializableExtra("hadeth");
//            toolbarTitle.setText(hadeth.getTitle());
            HadethActivity.this.getSupportActionBar().setTitle(hadeth.getTitle());

            hadethText.setText(hadeth.getText());
            hadethTeller.setText(hadeth.getTeller());


        }

    }

    //For back button
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }
}
