package com.azkarak.azkarak;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

public class ForZekrAndHadethFragmentActivity extends AppCompatActivity {

    ViewPager viewPager;
    TabLayout tabLayout;
    PagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_for_zekr_and_hadeth_fragment);



        Bundle bundleOne = new Bundle();
        Bundle bundleTwo = new Bundle();
        //For back button
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        try {

            String fourtiesAsString = getIntent().getExtras().getString("FOURTIES");
            String azkarAsString = getIntent().getExtras().getString("AZKAR");

            bundleOne.putString("AZKAR", azkarAsString);
            bundleTwo.putString("FOURTY", fourtiesAsString);

        } catch (Exception e) {
            e.printStackTrace();
        }

        viewPager = findViewById(R.id.pager_viewr_main);
        tabLayout = findViewById(R.id.tabs);
//        toolbar = findViewById(R.id.toolbar);
//        toolbarTitle = toolbar.findViewById(R.id.toolbar_title);
//
//        toolbarTitle.setText("الأذكار");
//        toolbarTitle.setTypeface(null, Typeface.BOLD);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
//            toolbar.setElevation(10);


        tabLayout.addTab(tabLayout.newTab().setIcon(getResources().getDrawable(R.drawable.doaa_1)));
        tabLayout.addTab(tabLayout.newTab().setIcon(getResources().getDrawable(R.drawable.hadith_1)));

        //link viewPager with tab layout
        tabLayout.setupWithViewPager(viewPager);
        adapter = new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), bundleOne, bundleTwo);
        viewPager.setAdapter(adapter);

        // to set the the right fragment for the start by category number
        viewPager.setCurrentItem(getIntent().getExtras().getInt("category"));

        int catNumber = getIntent().getExtras().getInt("category");
        if (catNumber == 0) {
            tabLayout.getTabAt(0).setIcon(getResources().getDrawable(R.drawable.doaa_2));
            ForZekrAndHadethFragmentActivity.this.getSupportActionBar().setTitle(R.string.azkar);

            tabLayout.getTabAt(1).setIcon(getResources().getDrawable(R.drawable.hadith_1));
//            toolbar.setTitle("الأذكار");
        } else if (catNumber == 1) {
            tabLayout.getTabAt(0).setIcon(getResources().getDrawable(R.drawable.doaa_1));
            tabLayout.getTabAt(1).setIcon(getResources().getDrawable(R.drawable.hadith_2));
            ForZekrAndHadethFragmentActivity.this.getSupportActionBar().setTitle(R.string.forties);

//            toolbar.setTitle("الاربعون النووية");
        }


        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int pos = tab.getPosition();
                if (pos == 0) {
                    tabLayout.getTabAt(0).setIcon(getResources().getDrawable(R.drawable.doaa_2));
                    tabLayout.getTabAt(1).setIcon(getResources().getDrawable(R.drawable.hadith_1));
                    ForZekrAndHadethFragmentActivity.this.getSupportActionBar().setTitle(R.string.azkar);

//                    toolbar.setTitle("الأذكار");
                } else if (pos == 1) {
                    tabLayout.getTabAt(0).setIcon(getResources().getDrawable(R.drawable.doaa_1));
                    tabLayout.getTabAt(1).setIcon(getResources().getDrawable(R.drawable.hadith_2));
                    ForZekrAndHadethFragmentActivity.this.getSupportActionBar().setTitle(R.string.forties);
//                    toolbar.setTitle("الاربعون النووية");
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        //---------------------------------------------------------------------------------------------------------------------------
        //DATABASE

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        getMenuInflater().inflate(R.menu.toolbar_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.main_toolbar_menu_search);
        final SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView =
                (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                PagerAdapter.changeAzkarAndAhadeth(newText);

                return false;
            }
        });

        MenuItemCompat.OnActionExpandListener expandListener = new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem menuItem) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                return true;
            }
        };
        MenuItemCompat.setOnActionExpandListener(searchItem, expandListener);

        return true;

    }

    //For back button
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        else if (item.getItemId() == R.id.main_toolbar_menu_search) {

        }

        return super.onOptionsItemSelected(item);

    }

}