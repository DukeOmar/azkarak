package com.azkarak.azkarak;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
public class AhadethFragment extends Fragment {

    ListView ahadethListView;
    private ArrayList<Forty> fourties ;
    private ArrayList<Forty> searchedFourties ;
    private fortyAdapter fortyAdapter ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment1,container,false);

        ahadethListView = view.findViewById(R.id.ahadeth_fragment_listview);

        String fourtiesAsString ;
        if (getArguments() != null) {
            Type listOfFourties = new TypeToken<List<Forty>>() {
            }.getType();
            fourtiesAsString = getArguments().getString("FOURTY");
            fourties = new Gson().fromJson(fourtiesAsString, listOfFourties) ;
            searchedFourties = new Gson().fromJson(fourtiesAsString, listOfFourties) ;
        }

        //Linking forty adapter with ahadethListView
        fortyAdapter = new fortyAdapter(getActivity(), searchedFourties);
        ahadethListView.setAdapter(fortyAdapter);


        ahadethListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent hadethIntent = new Intent(view.getContext(),HadethActivity.class);

                hadethIntent.putExtra("hadeth",searchedFourties.get(position));
                startActivity(hadethIntent);

            }
        });


        return  view;
    }

    public void changeList(String word){

        Log.e("LOGGG", "Changed : " + word);
        searchedFourties = search(word);
        fortyAdapter = new fortyAdapter(getActivity(), searchedFourties);
        Log.e("LOGGG", "Changed : " + searchedFourties.size());
        ahadethListView.setAdapter(fortyAdapter);

    }

    public ArrayList<Forty> search(String word) {

        if(TextUtils.isEmpty(word)) {
            return new ArrayList<>(fourties);
        }

        ArrayList<Forty> mySearchedArrayList = new ArrayList<>() ;
        char[] searchWord = word.toCharArray();
        ArrayList<char[]> elementsCharArrayList = new ArrayList<>();

        searchedFourties.clear();
        searchedFourties = new ArrayList<>(fourties) ;
        Log.e("SEARCHED", "OLD : " + fourties.size());
        Log.e("SEARCHED", "NEW : " + searchedFourties.size());

        Log.e("AAAAAA", String.valueOf((int) 'أ')) ;

        int arraySize = searchedFourties.size();
        for (int i = 0; i < arraySize ; i++) {
            String x = searchedFourties.get(i).getTitle();
            elementsCharArrayList.add(x.toCharArray());
        }

        char[][] elementsCharList = elementsCharArrayList.toArray(new char[0][]);

        for (int i = 0; i < arraySize ; i++) {

            int x = 0;
            for (int j = 0; j < searchedFourties.get(i).getTitle().length() ; j++) {

                if (arabicCheck(searchWord[x], elementsCharList[i][j] )) {
                    if (x == word.length() - 1) {
                        mySearchedArrayList.add(searchedFourties.get(i));
                        break;
                    }
                    x++;
                } else {
                    x = 0;
                }
            }
        }
        return mySearchedArrayList ;

    }

    //a function to check the Arabic Characters
    public Boolean arabicCheck(Character first, Character second) {

        int x = (int) first ;
        int y = (int) second ;

        Boolean a = x == y ;

        // أ case for
        Boolean b = ( x == 1569 || x == 1570 || x == 1571 || x == 1573 || x == 1574 || x == 1575
                || x == 1649 || x == 1650 || x == 1651 || x == 1652 || x == 1653 )
                && ( y == 1569 || y == 1570 || y == 1571 || y == 1573 || y == 1574 || y == 1575
                || y == 1649 || y == 1650 || y == 1651 || y == 1652 || y == 1653 ) ;

        // ي case for
        Boolean c = ( x == 1568 || x == 1574 || x == 1597 || x == 1598 || x == 1599 || x == 1609
                || x == 1610 || x == 1656 || x == 1740 || x == 1741 || x == 1742 || x == 1744 || x == 1745 )
                &&  ( y == 1568 || y == 1574 || y == 1597 || y == 1598 || y == 1599 || y == 1609
                || y == 1610 || y == 1656 || y == 1740 || y == 1741 || y == 1742 || y == 1744 || y == 1745 ) ;

        // و case for
        Boolean d = ( x == 1572 || x == 1608 || x == 1654 || x == 1655 || x == 1732 || x == 1733
                || x == 1734 || x == 1735 || x == 1736 || x == 1737 || x == 1738 || x == 1739 || x == 1743 )
                &&  ( y == 1572 || y == 1608 || y == 1654 || y == 1655 || y == 1732 || y == 1733
                || y == 1734 || y == 1735 || y == 1736 || y == 1737 || y == 1738 || y == 1739 || y == 1743 ) ;

        // ة case for
        Boolean e = ( x == 1577 || x == 1607 || x == 1726 || x == 1728 || x == 1729 || x == 1730
                || x == 1731 || x == 1749 || x == 1791 )
                &&  ( y == 1577 || y == 1607 || y == 1726 || y == 1728 || y == 1729 || y == 1730
                || y == 1731 || y == 1749 || y == 1791 ) ;

        return  a || b || c || d || e ;

    }

}
