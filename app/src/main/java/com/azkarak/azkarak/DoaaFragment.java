package com.azkarak.azkarak;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

public class DoaaFragment extends Fragment {

    private Doaa doaa;
    private TextView text;
    private TextView teller;
    private TextView number;
    private RelativeLayout innerRelativeLayout;
    private TextView count;
    private ViewPager viewPager;
    private Button numberButton;
    boolean isVibrateFirst = true;

    private int n = 0;
    private int doaas_total_number = 0;

    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.zekr_inner_layout, container, false);

        final Vibrator vibe = (Vibrator) getActivity().getBaseContext().getSystemService(Context.VIBRATOR_SERVICE);
        //replace yourActivity.this with your own activity or if you declared a context you can write context.getSystemService(Context.VIBRATOR_SERVICE);

        RelativeLayout relativeLayout = view.findViewById(R.id.zekr_inner_layout);
        relativeLayout.setRotation(180);

        text = view.findViewById(R.id.zekr_hadeth_text);
        teller = view.findViewById(R.id.zekr_hadeth_teller);
        number = view.findViewById(R.id.doaa_number_view);
        count = view.findViewById(R.id.zekr_counter_view);
        numberButton = view.findViewById(R.id.doaa_number_button);
        innerRelativeLayout = view.findViewById(R.id.zekr_inner_layout_top_relative_layout);
        viewPager = view.findViewById(R.id.pager_viewer_zekr);
        //this for initialize view pager with current
        //and we use getActivity() to get context from fragment
        viewPager = new ViewPager(getActivity().getBaseContext());

        if (getArguments() != null) {
            String doaaAsString = getArguments().getString("DOAA");
            doaa = new Gson().fromJson(doaaAsString, Doaa.class);
            try {
                doaas_total_number = Integer.parseInt(getArguments().getString("DOAAS_NUMBER"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        text.setText(doaa.getText());
        teller.setText(doaa.getTeller());
        numberButton.setText(String.valueOf(n));
        count.setText(doaa.getId() + 1 + " من " + doaas_total_number);
        number.setText(CountsAsString.getCountString(doaa.getNumber()));

        innerRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pagerCount = ((ZekrActivity) getActivity()).getCount();
                int position = ((ZekrActivity) getActivity()).getPager().getCurrentItem();
                Log.e("COUNT", String.valueOf(pagerCount));
                Log.e("POSITION", String.valueOf(position));

                //excuted when zekr is finished
                if (n == doaa.getNumber()) {
                    if (isVibrateFirst) {
                        Toast.makeText(getActivity(), "انتهى الذكر", Toast.LENGTH_SHORT).show();
                        vibe.vibrate(150);//100 represents the milliseconds (the duration of the vibration}
                        isVibrateFirst = false;
                    }

                } else if (n != doaa.getNumber() - 1) {
                    n++;
                    numberButton.setText(String.valueOf(n));
                } else if (position == pagerCount - 1) {
                    n++;
                    numberButton.setText(String.valueOf(n));
                } else {
                    n++;
                    numberButton.setText(String.valueOf(n));
                    position = ((ZekrActivity) getActivity()).getPager().getCurrentItem();
                    ((ZekrActivity) getActivity()).getPager().setCurrentItem(position + 1);
                    vibe.vibrate(150);//100 represents the milliseconds (the duration of the vibration}
                    n = 0;
                }
            }
        });

        numberButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pagerCount = ((ZekrActivity) getActivity()).getCount();
                int position = ((ZekrActivity) getActivity()).getPager().getCurrentItem();
                Log.e("COUNT", String.valueOf(pagerCount));
                Log.e("POSITION", String.valueOf(position));


                if (n == doaa.getNumber()) {

                } else if (n != doaa.getNumber() - 1) {
                    n++;
                    numberButton.setText(String.valueOf(n));
                } else if (position == pagerCount - 1) {
                    n++;
                    numberButton.setText(String.valueOf(n));
                } else {
                    n++;
                    numberButton.setText(String.valueOf(n));
                    numberButton.setEnabled(false);
                    position = ((ZekrActivity) getActivity()).getPager().getCurrentItem();
                    ((ZekrActivity) getActivity()).getPager().setCurrentItem(position + 1);
                    n = 0;
                }
            }
        });

        return view;
    }

}
