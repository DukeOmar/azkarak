package com.azkarak.azkarak;


import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ZekrActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView toolbarTitle;

    ViewPager viewPager;
    zekrPagerAdapter adapterForZekr;

    private Zekr zekr;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.doaa_activity);

        //For back button
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        viewPager = findViewById(R.id.doaa_activity_viewpager);
//        toolbar = findViewById(R.id.toolbar);
//        toolbarTitle = toolbar.findViewById(R.id.toolbar_title) ;

        if (getIntent().hasExtra("ZEKR"))
            zekr = (Zekr) getIntent().getSerializableExtra("ZEKR");

//        toolbarTitle.setText(zekr.getTitle());
//        toolbarTitle.setTypeface(null, Typeface.BOLD);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
//            toolbar.setElevation(10);

        adapterForZekr = new zekrPagerAdapter(getSupportFragmentManager(), zekr.getDoaas());
        viewPager.setAdapter(adapterForZekr);
        viewPager.setRotation(180);
        viewPager.setOffscreenPageLimit(zekr.getNumberOfDoaa()-1);

        ZekrActivity.this.getSupportActionBar().setTitle(zekr.getTitle());


    }


    //For back button
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

    public ViewPager getPager() {
        return viewPager;
    }

    public int getCount() {
        return adapterForZekr.getCount();
    }


}
